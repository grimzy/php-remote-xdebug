# PHP Remote XDebug

Bash script to run a PHP script on the command line with remote XDebug.

## Installation

Clone the repository:

```shell
git clone git@bitbucket.org:grimzy/php-remote-xdebug.git
```

Create a symlink in one of the directories in `$PATH`:

```bash
cd ~/bin
ln -s path/to/php-remote-xdebug/phpx
```

## Configuration

You can set the `idekey` by editing the `idekey` file.

## Usage

Simply use `phpx` instead of `php` when on the command line.

```shell
phpx <command|file>
```

Examples:

```shell
phpx console db:create
phpx index.php
```